'''
@Author: hua
@Date: 2019-10-08 14:54:03
@description: 
@LastEditors: hua
@LastEditTime: 2019-11-16 15:55:37
'''
from app import app
from app.Models.Msg import Msg
from app.Vendor.Decorator import transaction, validator
from app.Admin.Controllers.BaseController import BaseController
from app.Vendor.UsersAuthJWT import UsersAuthJWT


@app.route('/api/v2/admin/msg/list', methods=['POST'])
@validator(name="page_no", rules={'type': 'integer'}, default=0)
@validator(name="per_page", rules={'type': 'integer'}, default=15)
@validator(name="start_time", rules={'required': False, 'type': 'integer'})
@validator(name="end_time", rules={'required': False, 'type': 'integer'})
@validator(name="orderBy", rules={'type': 'string'}, default='create_at')
@validator(name="order", rules={'type': 'string'}, default='desc')
@UsersAuthJWT.AdminApiAuth
def msgList(*args, **kwargs):
    """ 获取消息列表 """
    params = kwargs['params']
    filters = set()
    if 'start_time' in params.keys():
        filters.add((Msg.created_at> params['start_time'] ))
    if 'end_time' in params.keys():
        filters.add((Msg.created_at< params['end_time'] ))
    data = Msg().getList(filters, params['orderBy']+" "+params['order'],(),params['page_no'], params['per_page'])
    return BaseController().successData(data)

@app.route('/api/v2/admin/msg/delete', methods=['POST'])
@validator(name="start_time", rules={'required': False, 'type': 'integer'})
@validator(name="end_time", rules={'required': False, 'type': 'integer'})
@transaction
@UsersAuthJWT.AdminApiAuth
def delMsgByTime(*args, **kwargs):
    """ 根据时间段删除消息 """
    params = kwargs['params']
    filters = set()
    if 'start_time' in params.keys():
        filters.add((Msg.created_at> params['start_time'] ))
    if 'end_time' in params.keys():
        filters.add((Msg.created_at< params['end_time'] ))
    data = Msg().delete(filters)
    return BaseController().successData(data)