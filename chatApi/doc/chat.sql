-- chat.ht_address_book definition

CREATE TABLE `ht_address_book` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `be_focused_user_id` varchar(32) NOT NULL COMMENT '被关注者用户编号',
  `focused_user_id` varchar(32) NOT NULL COMMENT '关注者用户编号',
  `created_at` int(11) unsigned NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '更新时间',
  `room_uuid` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '房间唯一编号',
  `save_action` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '保存方式',
  `is_alert` tinyint(1) NOT NULL COMMENT '是否提醒',
  `unread_number` int(11) unsigned NOT NULL COMMENT '未读取信息次数',
  `is_input` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否在输入',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '0是普通用户，1是后台用户',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `focused_user_id` (`focused_user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1471 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;


-- chat.ht_admin definition

CREATE TABLE `ht_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT 'admin888',
  `pwd` char(32) NOT NULL DEFAULT '' COMMENT 'admin',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(30) NOT NULL DEFAULT '' COMMENT '邮箱',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户状态 0：正常； 1：禁用 ；2：未验证',
  `login_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `delete_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  `nick_name` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `uuid` varchar(32) NOT NULL DEFAULT '' COMMENT '唯一编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员表';


-- chat.ht_config definition

CREATE TABLE `ht_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '名字',
  `type` varchar(50) NOT NULL COMMENT '类型',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `code` varchar(50) NOT NULL COMMENT '编号',
  `config` text NOT NULL COMMENT '配置',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `created_at` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  `updated_at` int(11) unsigned DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;


-- chat.ht_logs definition

CREATE TABLE `ht_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '类型，1是普通接口日志',
  `level` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '报错等级，1是debug，2是warn，3是error',
  `data` text CHARACTER SET utf8 NOT NULL COMMENT '内容',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;


-- chat.ht_msg definition

CREATE TABLE `ht_msg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '名字',
  `msg` text NOT NULL COMMENT '聊天内容',
  `room_uuid` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '房间唯一编号',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户编号',
  `type` tinyint(2) unsigned NOT NULL COMMENT '类型',
  `head_img` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '头像',
  `created_at` bigint(14) unsigned NOT NULL COMMENT '创建时间',
  `send_status` tinyint(2) NOT NULL COMMENT '发送状态',
  `read_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '读取状态',
  `user_type` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3745 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;


-- chat.ht_room definition

CREATE TABLE `ht_room` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `room_uuid` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '房间编号',
  `last_msg` text NOT NULL COMMENT '最后一条消息',
  `updated_at` int(11) unsigned NOT NULL COMMENT '最后一条消息时间',
  `created_at` int(11) unsigned NOT NULL COMMENT '创建时间',
  `type` int(11) unsigned NOT NULL COMMENT '类型，0是单聊，1是群聊，2是通知，3是管理员聊天',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '房间名',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `room_uuid` (`room_uuid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1395 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;


-- chat.ht_user_room_relation definition

CREATE TABLE `ht_user_room_relation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户编号',
  `room_uuid` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '房间编号',
  `save_action` tinyint(1) NOT NULL DEFAULT '0' COMMENT '保存方式',
  `is_alert` tinyint(1) unsigned NOT NULL COMMENT '是否提醒',
  `unread_number` int(11) unsigned NOT NULL COMMENT '未读取信息次数',
  `created_at` int(11) unsigned NOT NULL COMMENT '创建时间',
  `updated_at` int(11) unsigned NOT NULL COMMENT '更新时间',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0是群员，1是管理员，2是群主',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0是正常，1是禁言',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uuid` (`user_id`,`room_uuid`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=601 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;


-- chat.ht_users definition

CREATE TABLE `ht_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `email` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '邮箱',
  `password` varchar(500) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '密码',
  `nick_name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '昵称',
  `head_img` varchar(255) NOT NULL COMMENT '头像',
  `online` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '在线状态',
  `first_word` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '首字母',
  `updated_at` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created_at` int(11) unsigned NOT NULL COMMENT '创建时间',
  `balance` decimal(10,0) DEFAULT '0' COMMENT '余额',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11786 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;


-- chat.th_red_pack_record definition

CREATE TABLE `th_red_pack_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `red_package_id` int(11) unsigned NOT NULL,
  `price` decimal(10,0) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- chat.th_red_package definition

CREATE TABLE `th_red_package` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

